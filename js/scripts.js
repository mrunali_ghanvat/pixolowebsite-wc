/***************** Waypoints ******************/

$(document).ready(function () {

    $('.wp1').waypoint(function () {
        $('.wp1').addClass('animated fadeInLeft');
    }, {
        offset: '75%'
    });
    $('.wp2').waypoint(function () {
        $('.wp2').addClass('animated fadeInUp');
    }, {
        offset: '75%'
    });
    $('.wp3').waypoint(function () {
        $('.wp3').addClass('animated fadeInDown');
    }, {
        offset: '55%'
    });
    $('.wp4').waypoint(function () {
        $('.wp4').addClass('animated fadeInDown');
    }, {
        offset: '75%'
    });
    $('.wp5').waypoint(function () {
        $('.wp5').addClass('animated fadeInUp');
    }, {
        offset: '75%'
    });
    $('.wp6').waypoint(function () {
        $('.wp6').addClass('animated fadeInDown');
    }, {
        offset: '75%'
    });

});

/***************** Slide-In Nav ******************/

$(window).load(function () {

    $('.nav_slide_button').click(function () {
        $('.pull').slideToggle();
    });

});

/***************** Smooth Scrolling ******************/

$(function () {

    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 2000);
                return false;
            }
        }
    });

});

/***************** Nav Transformicon ******************/

/*document.querySelector("#nav-toggle").addEventListener("click", function() {
	this.classList.toggle("active");
});*/

/***************** Overlays ******************/

$(document).ready(function () {
    if (Modernizr.touch) {
        // show the close overlay button
        $(".close-overlay").removeClass("hidden");
        // handle the adding of hover class when clicked
        $(".img").click(function (e) {
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".close-overlay").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".img").hasClass("hover")) {
                $(this).closest(".img").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".img").mouseenter(function () {
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function () {
                $(this).removeClass("hover");
            });
    }
});

/***************** Flexsliders ******************/

$(window).load(function () {

    $('#portfolioSlider').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: true,
        touch: false,
        pauseOnHover: true,
        start: function () {
            $.waypoints('refresh');
        }
    });

    $('#servicesSlider').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: true,
        touch: true,
        pauseOnHover: true,
        start: function () {
            $.waypoints('refresh');
        }
    });

    $('#teamSlider').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: true,
        touch: true,
        pauseOnHover: true,
        start: function () {
            $.waypoints('refresh');
        }
    });

});



/***************************************WORLD CUP FIXTURES SCRIPT*********************************************/









$(window).load(function () {


    var matchinfo = []; //To Store All matches from todays date to last date
    var fixture = []; //To store todays match
    var todaysdate = new Date(new Date().toDateString());
    //Compare the dates of matches to get latest matches for group and knockout matches 
    var comparetimefunction = function (matchdata) {

        for (var matchindex in matchdata.matches) {
            var match = matchdata.matches[matchindex];
            var matchdate = new Date(match.date);
            if ((todaysdate.getTime() - 5400000) <= matchdate.getTime() && matchdate.getTime() <= (todaysdate.getTime() + 91800000)) {
                match.time = matchdate.getTime();
                matchinfo.push(match);
                matchinfo[matchinfo.length - 1].matchtype = matchdata.name;
            }
        }
    };
    //

    /*Get json data */

    $.ajax({
        url: "https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json",
        dataType: 'json',
        success: function (result) {
            console.log(result);
            //Get group matches
            for (var property in result.groups) {
                comparetimefunction(result.groups[property]);

            };
            //Get knockout matches
            for (var property in result.knockout) {
                comparetimefunction(result.knockout[property]);
            }

            //Sort matches according to time and dates
            matchinfo = matchinfo.sort((a, b) => a.time - b.time);




            //            var datetocompare = new Date(matchinfo[0].date).toLocaleDateString();
            console.log(matchinfo);
            //            for (var matchindex = 0; matchindex < 3; matchindex++) {
            for (var matchindex in matchinfo) {

                var homeindex = -1,
                    awayindex = -1;
                fixture[matchindex] = matchinfo[matchindex];
                homeindex = result.teams.findIndex((team) => fixture[matchindex].home_team == team.id); //Get team data from teams by ID
                awayindex = result.teams.findIndex((team) => fixture[matchindex].away_team == team.id); //Get team data from teams by ID
                console.log(fixture);
                var time = new Date(fixture[matchindex].date).toLocaleTimeString();
                time = time.replace(time.substr(time.lastIndexOf(':'), 3), '');



                $('.wc-main-wrapper').append(`<div class="score-wrapper">
                    <div class="fixture-wrapper">
                        <div class="flag-wrapper">
                            <img src="` + (homeindex != -1 ? result.teams[homeindex].flag : '') + `" id="home-team-flag" class="flag-img">
                        </div>
                        <div class="team-name-wrapper">
                            <p class="team-name score-text" id="home-team-name">` + (homeindex != -1 ? result.teams[homeindex].name : fixture[matchindex].home_team) + `</p>
                        </div>
                        <div class="vs-wrapper">
                            <p class="vs-text score-text" id="result` + matchindex + `">` + ((fixture[matchindex].home_result || fixture[matchindex].away_result) ? (fixture[matchindex].home_result + "-" + fixture[matchindex].away_result) : 'VS') + `</p>
                        </div>
                        <div class="team-name-wrapper away-team-name-wrapper">
                            <p class="team-name score-text" id="away-team-name">` + (awayindex != -1 ? result.teams[awayindex].name : fixture[matchindex].away_team) + `</p>
                        </div>
                        <div class="flag-wrapper away-flag-wrapper">
                            <img src="` + (awayindex != -1 ? result.teams[awayindex].flag : '') + `" id="away-team-flag" class="flag-img">
                        </div>
                    </div>
                    <div class="schedule-wrapper">
                        <div class="shedule-name-wrapper">
                            <p class="schedule-text">` + fixture[matchindex].matchtype + ` ` + new Date(fixture[matchindex].date).toLocaleDateString() + ' - ' + time + `</p>
                        </div>
                        <div class="time-wrapper">
                           <button class="match-details-button" onclick="window.location.assign('http://www.pixoloproductions.com/matchdetails/` + fixture[matchindex].name + `')">Match Details</button>
                        </div>
                    </div>
                </div>`);






            };




            setInterval(function () {
                var count = 0;
                fixture.forEach((element) => {


                    $.ajax({
                        url: "https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json",
                        dataType: 'json',
                        success: function (results) {
                            var indexofmatches = -1;
                            for (var prop in results.groups) {
                                if (indexofmatches == -1) {
                                    indexofmatches = results.groups[prop].matches.findIndex((match) => match.name == element.name);

                                } else {
                                    element.finished = results.groups[prop].matches[indexofmatches].finished;
                                    element.home_result = results.groups[prop].matches[indexofmatches].home_result;
                                    element.away_result = results.groups[prop].matches[indexofmatches].away_result;

                                    break;

                                }

                            }
                            for (var prop in results.knockout) {
                                if (indexofmatches == -1) {
                                    indexofmatches = results.knockout[prop].matches.findIndex((match) => match.name == element.name);


                                } else {
                                    element.finished = results.knockout[prop].matches[indexofmatches].finished;
                                    element.home_result = results.knockout[prop].matches[indexofmatches].home_result;
                                    element.away_result = results.knockout[prop].matches[indexofmatches].away_result;

                                    break;
                                }

                            }


                            if (!element.finished) {
                                if (element.home_result || element.away_result) {
                                    document.getElementById('result' + count).innerHTML = element.home_result + "-" + element.away_result;
                                }




                            }


                        }

                    })
                    count++;

                })
            }, 50000);

            $('.wc-main-wrapper').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                centerMode: true,
                responsive: [{
                    breakpoint: 400,
                    settings: {
                        arrows: false,
                        autoplay: true,
                        autoplaySpeed: 2000,

                    }
               }]




            });

        }


    })
});