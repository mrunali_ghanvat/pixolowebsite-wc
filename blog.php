<!DOCTYPE html>
<html>
<title>PHP</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Lato", sans-serif;
    }

    body,
    html {
        height: 100%;
        color: #777;
        line-height: 1.8;
    }

    /* Create a Parallax Effect */

    .bgimg-1,
    .bgimg-2,
    .bgimg-3 {
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    /* First image (Logo. Full height) */

    .bgimg-1 {
        background-image: url('img/augmented.jpg');
        /* <?php echo $thisblog->image; ?> */
        min-height: 100%;
    }

    /* Second image (Portfolio) */

    .bgimg-2 {
        background-image: url("img/augmented.jpg");
        /* <?php echo $thisblog->image; ?> */
        min-height: 400px;
        position: relative;
    }

    .bgimg-2:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(29, 29, 29, 0.74);
    }

    .also-see-thumb {
        cursor: pointer;
        margin-bottom: 15px;
    }

    .w3-wide {
        letter-spacing: 10px;
    }

    .w3-hover-opacity {
        cursor: pointer;
    }

    .go-back-btn {
        float: right !important;
    }

    .social-main-wrap {
        background-color: #fbf8f8 !important;
    }

    .share-text {
        margin: 0;
        text-align: center;
        font-size: 20px;
        color: #3f3f3f;
        font-weight: 600;
        letter-spacing: 2px;
        text-transform: uppercase;
        padding-top: 18px;
    }

    .social-wrapper {
        display: flex;
        justify-content: space-around;
        background-color: #fbf8f8 !important;
        align-items: center;
        width: 80%;
        margin: auto;
    }

    .share-btn-wrapper {
        cursor: pointer;
    }

    .share-btn-wrapper:hover svg {
        transform: rotateZ(50deg);
    }

    .share-btn-wrapper svg {
        transition: 0.3s all;
        width: 60px;
        height: 60px;
    }

    /* Turn off parallax scrolling for tablets and phones */

    @media only screen and (max-device-width: 1024px) {
        .w3-xlarge {
            font-size: 22px!important;
        }
        .w3-display-middle-comments {
            position: relative;
            top: 0;
            left: 0;
            transform: none;
        }
    }

</style>

<body>

    <!-- Navbar (sit on top) -->
    <div class="w3-top">
        <div class="w3-bar" id="myNavbar">
            <a href="http://www.pixoloproductions.com/" class="w3-bar-item w3-button go-back-btn">back to <img src="http://pixoloproductions.com/img/pixolo_logo.png" width="90px" /></a>
        </div>
    </div>

    <!-- First Parallax Image with Logo Text -->
    <div class="bgimg-1 w3-display-container w3-opacity-min" id="home">
        <div class="w3-display-middle">
            <span class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">/*<?php echo $thisblog->title; ?>*/ TITLE</span>
        </div>
    </div>

    <!-- Container (About Section) -->
    <div class="w3-content w3-container w3-padding-64" id="about">
        <h3 class="w3-center">BY: JYOTI PANDEY /*
            <?php echo $thisblog->author; ?>*/</h3>


        <div id="blog-contents">
/*<?php echo $thisblog->blog; ?>*/
        </div>


        <p><em><span>This short article is the first of a 3 part series of blogs and is targeted towards Educators and Parents who want to embrace the future and ensure that their students and kids learn in the best possible way</span></em></p>
        <p><span>What is the purpose of education? Do we really learn anything in schools? What is the future of work? Are machines and algorithms going to take away all our jobs? Words like EdTech, Augmented Reality, Virtual Reality, Mixed Reality have been doing rounds – Do we really understand what it means and is it relevant for us as educators &amp; parents</span></p>
        <p><span>The role of education has never been questioned more – and the importance of learning has never been felt more. On an average, current generation kids learn things at least at an age 5 years lesser than their grandparents. And more than the number, the important thing is that lot more is packaged in the growing minds of these students. Do they really understand it? Is this the right approach?</span></p>
        <p><span>Research says that childhood is the best age to <g class="gr_ gr_44 gr-alert gr_gramm gr_inline_cards gr_run_anim Punctuation only-del replaceWithoutSep" id="44" data-gr-id="44">learn,</g> when the brain is the best sponge and therefore expecting kids to learn as much as possible is not technically wrong. But at the same time, many parents complain that kids have become more fickle minded, more playful with lower <g class="gr_ gr_38 gr-alert gr_spell gr_inline_cards gr_run_anim ContextualSpelling ins-del" id="38" data-gr-id="38">patience</g>. With mobiles and smart devices, there are three major trends which are happening:</span></p>
        <ul>
            <li><strong><span>Shortening Attention-Span:</span></strong><span> Test matches have become T20s, Newspapers have become Inshorts. What is the future of Youtube for Education then? GIFs?</span></li>
            <li><strong><span>Outsourcing the thinking:</span></strong><span> When Google has millions of answers at your fingertips provided in <g class="gr_ gr_208 gr-alert gr_gramm gr_inline_cards gr_run_anim Grammar only-ins doubleReplace replaceWithoutSep" id="208" data-gr-id="208">fraction</g> of seconds, what is the need to think? What is the need to solve a problem on your own? This approach has the danger of making the next generation of students to be great sponges of information, but very poor fountainheads of new ideas. </span></li>
        </ul>
        <p><span>&nbsp;</span></p>
        <p><span>So what does it means for today’s educators? Is technology bad? Should we shun mobiles and keep them out of sight of kids:</span></p>
        <p><span>Whether we like it or not, the future is for those who constructively leverage technology. Please note the word – constructively. This word ‘constructively’ will separate those who become slaves of technologies and those who meaningfully leverage it. And what is the one most important mantra for this: <strong>Make everything visual &amp; engaging - </strong>I hear and I forget. I see and I remember. I do and I understand. </span></p>
        <p><span>In the next part of this blog, we will describe how exactly can parents ensure that the usage of technology is constructively used to make the learning more visual &amp; engaging</span></p>






    </div>
    <div class="social-main-wrap">
        <p class="share-text">because Sharing is caring</p>
        <div class="w3-row w3-center w3-dark-grey w3-padding-16 social-wrapper">
            <div class="w3-quarter w3-section share-btn-wrapper">
                <a href="https://www.facebook.com/login.php?skip_api_login=1&api_key=966242223397117&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fshare.php%3Fu%3Dhttp%253A%252F%252Fwww.argorithm.com%252Fblog%252Fwhat-are-the-trends-that-are-the-changing-Education-1of3.html%26title%3DWhat%2Bare%2Bthe%2Btrends%2Bthat%2Bare%2Bthe%2Bchanging%2BEducation%253F%2B%25281%252F3%2529&cancel_url=https%3A%2F%2Fwww.facebook.com%2Fdialog%2Fclose_window%2F%3Fapp_id%3D966242223397117%26connect%3D0%23_%3D_&display=popup&locale=en_GB" target="_blank">
                <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="56.098" cy="56.098" r="56.098" fill="#3B5998"></circle>
                            <path d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34   c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z" fill="#fff"></path>
                        </svg>
                        </a>
            </div>
            <div class="w3-quarter w3-section share-btn-wrapper">
               <!--PHP-->
                <a href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3DWhat%2520are%2520the%2520trends%2520that%2520are%2520the%2520changing%2520Education%3F%2520%281%2F3%29%2Bhttp%3A%2F%2Fwww.argorithm.com%2Fblog%2Fwhat-are-the-trends-that-are-the-changing-Education-1of3.html" target="_blank">
                <svg enable-background="new 0 0 112.197 112.197" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.099" cy="56.098" r="56.098" fill="#55ACEE"></circle>
                        <path d="m90.461 40.316c-2.404 1.066-4.99 1.787-7.702 2.109 2.769-1.659 4.894-4.284 5.897-7.417-2.591 1.537-5.462 2.652-8.515 3.253-2.446-2.605-5.931-4.233-9.79-4.233-7.404 0-13.409 6.005-13.409 13.409 0 1.051 0.119 2.074 0.349 3.056-11.144-0.559-21.025-5.897-27.639-14.012-1.154 1.98-1.816 4.285-1.816 6.742 0 4.651 2.369 8.757 5.965 11.161-2.197-0.069-4.266-0.672-6.073-1.679-1e-3 0.057-1e-3 0.114-1e-3 0.17 0 6.497 4.624 11.916 10.757 13.147-1.124 0.308-2.311 0.471-3.532 0.471-0.866 0-1.705-0.083-2.523-0.239 1.706 5.326 6.657 9.203 12.526 9.312-4.59 3.597-10.371 5.74-16.655 5.74-1.08 0-2.15-0.063-3.197-0.188 5.931 3.806 12.981 6.025 20.553 6.025 24.664 0 38.152-20.432 38.152-38.153 0-0.581-0.013-1.16-0.039-1.734 2.622-1.89 4.895-4.251 6.692-6.94z" fill="#F1F2F2"></path>
                    </svg>
                </a>
            </div>
            <div class="w3-quarter w3-section share-btn-wrapper">
               <!--PHP-->
                <a href="www.linkedin.com/shareArticle?mini=true&url=http://www.argorithm.com/blog/what-are-the-trends-that-are-the-changing-Education-1of3.html&title=What%20are%20the%20trends%20that%20are%20the%20changing%20Education?%20(1/3)&source=http://argorithm.com" target="_blank">
                <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.098" cy="56.097" r="56.098"></circle>
                        <path d="m89.616 60.611v23.128h-13.409v-21.578c0-5.418-1.936-9.118-6.791-9.118-3.705 0-5.906 2.491-6.878 4.903-0.353 0.862-0.444 2.059-0.444 3.268v22.524h-13.41s0.18-36.546 0-40.329h13.411v5.715c-0.027 0.045-0.065 0.089-0.089 0.132h0.089v-0.132c1.782-2.742 4.96-6.662 12.085-6.662 8.822 0 15.436 5.764 15.436 18.149zm-54.96-36.642c-4.587 0-7.588 3.011-7.588 6.967 0 3.872 2.914 6.97 7.412 6.97h0.087c4.677 0 7.585-3.098 7.585-6.97-0.089-3.956-2.908-6.967-7.496-6.967zm-6.791 59.77h13.405v-40.33h-13.405v40.33z" fill="#fff"></path>
                    </svg>
                </a>
            </div>
            <div class="w3-quarter w3-section share-btn-wrapper">
               <!--PHP-->
                <a href="whatsapp://send?text=http://www.argorithm.com/blog/what-are-the-trends-that-are-the-changing-Education-1of3.html" target="_blank">
                <svg enable-background="new 0 0 418.135 418.135" version="1.1" viewBox="0 0 418.14 418.14" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                        <g fill="#7AD06D">
                                           <path d="m198.93 0.242c-110.43 5.258-197.57 97.224-197.24 207.78 0.102 33.672 8.231 65.454 22.571 93.536l-22.017 106.87c-1.191 5.781 4.023 10.843 9.766 9.483l104.72-24.811c26.905 13.402 57.125 21.143 89.108 21.631 112.87 1.724 206.98-87.897 210.5-200.72 3.771-120.94-96.047-219.55-217.41-213.77zm124.96 321.96c-30.669 30.669-71.446 47.559-114.82 47.559-25.396 0-49.71-5.698-72.269-16.935l-14.584-7.265-64.206 15.212 13.515-65.607-7.185-14.07c-11.711-22.935-17.649-47.736-17.649-73.713 0-43.373 16.89-84.149 47.559-114.82 30.395-30.395 71.837-47.56 114.82-47.56 43.372 1e-3 84.147 16.891 114.82 47.559 30.669 30.669 47.559 71.445 47.56 114.82-1e-3 42.986-17.166 84.428-47.561 114.82z"></path>
                                            <path d="m309.71 252.35-40.169-11.534c-5.281-1.516-10.968-0.018-14.816 3.903l-9.823 10.008c-4.142 4.22-10.427 5.576-15.909 3.358-19.002-7.69-58.974-43.23-69.182-61.007-2.945-5.128-2.458-11.539 1.158-16.218l8.576-11.095c3.36-4.347 4.069-10.185 1.847-15.21l-16.9-38.223c-4.048-9.155-15.747-11.82-23.39-5.356-11.211 9.482-24.513 23.891-26.13 39.854-2.851 28.144 9.219 63.622 54.862 106.22 52.73 49.215 94.956 55.717 122.45 49.057 15.594-3.777 28.056-18.919 35.921-31.317 5.362-8.453 1.128-19.679-8.494-22.442z"></path>
                                          </g>
                                        </svg>
                </a>
            </div>
        </div>
    </div>

    <!-- Second Parallax Image with Portfolio Text -->
    <div class="bgimg-2 w3-display-container w3-opacity-min">
        <div class="w3-display-middle w3-display-middle-comments">
            <div class="fbcomment-div">
                <div class="fb-comments" data-href="http://argorithm.com/fbcomments/blog/what-are-the-trends-that-are-the-changing-education-1of3" data-width="1000px" data-numposts="5"></div>
                <div id="fb-root"></div>
                <!--PHP-->
                <script>
                    (function(d, s, id) {
                        console.log(id);
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=153203914724487&autoLogAppEvents=1';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));

                </script>
            </div>
            <!-- FB Comments ends here -->
        </div>
    </div>


    <div class="w3-content w3-container w3-padding-64">
        <h3 class="w3-center">ALSO CHECK THIS OUT</h3>

        <div class="w3-row-padding w3-center">
<!--WHILE FETCHING BLOGS FOR HERE, LIMIT TO 8-->
            /*
            <?php foreach($otherblogs as $blog) 
                {
            ?>
            <div class="w3-col m3 also-see-thumb" data-url="http://www.pixoloproductions.com/blog/<?php echo $blog->url; ?>">
                <img src="<?php echo $blog->image; ?>" style="width:100%" class="w3-hover-opacity" alt="<?php echo $blog->title; ?>">
                <p class="thumbnail-title">
                    <?php echo $blog->title; ?>
                </p>
            </div>
            <?php
                }
            ?>
                */

                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>
                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>
                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>

                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>

                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>

                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>

                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>

                <div class="w3-col m3 also-see-thumb" data-url="https://www.google.com">
                    <img src="img/augmented.jpg" style="width:100%" class="w3-hover-opacity" alt="">
                    <p class="thumbnail-title">What are the trends that are the changing Education? (1/3)</p>
                </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="w3-center w3-black w3-padding-64 w3-opacity w3-hover-opacity-off">
        <div class="w3-xlarge w3-section">
            <a href="https://www.facebook.com/PixoloProductions"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
            <a href="PHP"><i class="fa fa-instagram w3-hover-opacity"></i></a>
            <a href="PHP"><i class="fa fa-twitter w3-hover-opacity"></i></a>
            <a href="https://www.linkedin.com/company/pixolo-productions---india?originalSubdomain=in"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
        </div>
    </footer>


    <script>
        // Change style of navbar on scroll
        window.onscroll = function() {
            myFunction()
        };

        function myFunction() {
            var navbar = document.getElementById("myNavbar");
            if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
                navbar.className = "w3-bar" + " w3-card" + " w3-animate-top" + " w3-white";
            } else {
                navbar.className = navbar.className.replace(" w3-card w3-animate-top w3-white", "");
            }
        };

        var thmbs = document.getElementsByClassName('also-see-thumb');
        var url;
        for (var a = 0; a < thmbs.length; a++) {
            url = thmbs[a].getAttribute('data-url');
            thmbs[a].addEventListener("click", function() {
                window.open(url);
            });
        };

    </script>

</body>

</html>
